package entities.woker.attributes;

/**
 * Possible worker statuses
 */
public enum Status {
    FIRED,
    PROBATION,
    HIRED,
    REGULAR,
    RECOMMENDED_FOR_PROMOTION,
}
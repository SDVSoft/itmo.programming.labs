package entities.woker.attributes;

/**
 * Possible positions
 */
public enum Position {
    CLEANER,
    LABORER,
    HUMAN_RESOURCES,
    LEAD_DEVELOPER,
    HEAD_OF_DEPARTMENT,
}

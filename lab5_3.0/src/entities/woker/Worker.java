package entities.woker;

import entities.common.attributes.Coordinates;
import entities.organization.Organization;
import entities.organization.attributes.OrganizationType;
import entities.woker.attributes.Position;
import entities.woker.attributes.Status;

import javax.jnlp.IntegrationService;

public class Worker implements Comparable<Worker> {
    private int id; //Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
    private String name; //Поле не может быть null, Строка не может быть пустой
    private Coordinates coordinates; //Поле не может быть null
    private java.time.ZonedDateTime creationDate; //Поле не может быть null, Значение этого поля должно генерироваться автоматически
    private int salary; //Значение поля должно быть больше 0
    private java.time.LocalDate startDate; //Поле не может быть null
    private Position position; //Поле может быть null
    private Status status; //Поле может быть null
    private Organization organization; //Поле не может быть null

    //TODO
    /*
     * Creates a new City with given parameters
     * @param id - id of the City. Must be greater than 0
     * @param name - name of the City. Can't be null or empty string
     * @param coordinates - coordinates of the City. Can't be null
     * @param area - area of the City. Must be greater than 0
     * @param population - population of the City. Must be greater than 0
     * @param metersAboveSeaLevel - height above sea level of the City
     * @param agglomeration - agglomeration of the City
     * @param climate - type of climate in the City. Can't be null
     * @param standardOfLiving - living standard in the City. Can't be null
     * @param governor - governor of the City
     * @throws InvalidCityDataException if any parameter has an invalid value
     * @throws NullPointerException if any parameter that can't be null is actually null
     */
    public Worker(int id,
                  String name,
                  Coordinates coordinates,
                  int salary,
                  java.time.LocalDate startDate,
                  Position position,
                  Status status,
                  Organization organization)
            throws IllegalArgumentException, NullPointerException {
        this(id, name, coordinates, salary, startDate, position,
                status, organization, java.time.ZonedDateTime.now());
    }

    /*
     * Creates a new City with given parameters
     * @param id - id of the City. Must be greater than 0
     * @param name - name of the City. Can't be null or empty string
     * @param coordinates - coordinates of the City. Can't be null
     * @param area - area of the City. Must be greater than 0
     * @param population - population of the City. Must be greater than 0
     * @param metersAboveSeaLevel - height above sea level of the City
     * @param agglomeration - agglomeration of the City
     * @param climate - type of climate in the City. Can't be null
     * @param standardOfLiving - living standard in the City. Can't be null
     * @param governor - governor of the City
     * @param creationDate - initialization date of this City. Can't be null
     * @throws InvalidCityDataException if any parameter has an invalid value
     * @throws NullPointerException if any parameter that can't be null is actually null
     */
    public Worker(int id,
                  String name,
                  Coordinates coordinates,
                  int salary,
                  java.time.LocalDate startDate,
                  Position position,
                  Status status,
                  Organization organization,
                  java.time.ZonedDateTime creationDate)
            throws IllegalArgumentException, NullPointerException {
        if (id > 0)
            this.id = id;
        else
            throw new IllegalArgumentException();
        setName(name);
        setCoordinates(coordinates);
        setSalary(salary);
        setStartDate(startDate);
        setPosition(position);
        setStatus(status);
        setOrganization(organization);
        if (creationDate != null)
            this.creationDate = creationDate;
        else
            throw new NullPointerException("Поле City.creationDate не может иметь значение null.");
    }

    /**
     * Creates a copy of the specified City with new id.
     * @param id - id for newly created City
     * @param other - City to be copied
     */
    public Worker(int id, Worker other) {
        if (id > 0)
            this.id = id;
        else
            throw new IllegalArgumentException();
        this.name = other.name;
        this.coordinates = other.coordinates.clone();
        this.salary = other.salary;
        this.startDate = other.startDate;
        this.position = other.position;
        this.status = other.status;
        this.organization = other.organization;
        this.creationDate = other.creationDate;
    }

    /**
     * Returns a string representation of this City
     * @return A string representation of this City
     */
    public String toString() { return name + " (id:" + id + ")"; }

    /**
     * Returns a detailed string representation of this City
     * @return A detailed string representation of this City
     */
    public String getFullDescription() {
        StringBuilder fullDescription = new StringBuilder();
        fullDescription.append(toString());
        fullDescription.append("\n\tКоординаты:\n\t\t").append(coordinates);
        fullDescription.append("\n\tSalary:\n\t\t").append(salary);
        fullDescription.append("\n\tStart date:\n\t\t").append(startDate);
        if (position != null)
            fullDescription.append("\n\tPosition:\n\t\t").append(position);
        if (status != null)
            fullDescription.append("\n\tStatus:\n\t\t").append(status);
        String orgFullDescr = organization.getFullDescription();
        orgFullDescr = orgFullDescr.replace('\n', ' ');
        fullDescription.append("\n\tOrganization:\n\t\t").append(orgFullDescr);
        fullDescription.append("\n\tCreation Date:\n\t\t").append(creationDate);
        return fullDescription.toString();
    }

    /**
     * Compares this City to the specified object. The result is true if and only
     * if the argument is not null and is a City object that represents a city with the
     * same size (population, area, agglomeration) as this object
     * (population.equals(other.population) && area == other.area &&
     * agglomeration.equals(other.agglomeration)).
     * @param obj - The object to compare this City against
     * @return true if the given object represents a City of equivalent size to this
     * City, false otherwise
     */
    public boolean equals(Object obj) {
        //Criteria of equality: position, startDate, salary
        if (obj instanceof Worker) {
            Worker other = (Worker) obj;
            return position == other.position &&
                   startDate.equals(other.startDate) &&
                   salary == other.salary;
        }
        return false;
    }

    /**
     * Returns a hash code for this City. The hash code for a City object is computed as
     * Integer.hashCode(population.hashCode() + Float.hashCode(area) + agglomeration.hashCode())
     * @return a hash code value for this object.
     */
    public int hashCode() {
        int hashSum = position.hashCode() + startDate.hashCode() + Integer.hashCode(salary);
        return Integer.hashCode(hashSum);
    }

    /**
     * Compares this City to another. Cities are ordered first by population then by area
     * and the by agglomeration. The result is a positive integer if this City object
     * follows the argument City. The result is zero if the cities are equal; compareTo
     * returns 0 exactly when the equals(Object) method would return true. The result is a
     * negative integer if this City object precedes the argument City.
     * @param other - the City to be compared.
     * @return the value 0 if the argument City is equal to this City; a value less than 0
     * if this City is less than the City argument; and a value greater than 0 if this
     * City is greater than the City argument.
     */
    public int compareTo(Worker other) {
        //Comparison order: position -> -startDate -> salary
        int result = position.compareTo(other.position);
        if (result == 0) {
            result = -startDate.compareTo(other.startDate);
            if (result == 0)
                return Integer.compare(salary, other.salary);
        }
        return result;
    }

    /**
     * Returns id of this City
     * @return id of this City
     */
    public int getId() { return id; }

    /**
     * Returns name of this City
     * @return name of this City
     */
    public String getName() { return name; }

    /**
     * Returns clone of coordinates of this City
     * @return clone of coordinates of this City
     */
    public Coordinates getCoordinates() { return coordinates.clone(); }

    /**
     * Returns area of this City
     * @return area of this City
     */
    public int getSalary() { return salary; }

    /**
     * Returns population of this City
     * @return population of this City
     */
    public java.time.LocalDate getStartDate() { return startDate; }

    /**
     * Returns height above sea level of this City
     * @return height above sea level of this City
     */
    public Position getPosition() { return position; }

    /**
     * Returns agglomeration of this City
     * @return agglomeration of this City
     */
    public Status getStatus() { return status; }

    /**
     * Returns type of climate of this City
     * @return type of climate of this City
     */
    public Organization getOrganization() { return organization; }

    /**
     * Returns clone of initialization date of this City
     * @return clone of initialization date of this City
     */
    public java.time.ZonedDateTime getCreationDate() { return creationDate; }

    /*
     * Sets a new name of this City
     * @param name - a new name
     * @throws NullPointerException if name has null value
     * @throws InvalidCityNameException if name is an empty string
     */
    public void setName(String name) throws NullPointerException, IllegalArgumentException {
        if (name == null)
            throw new NullPointerException("Поле Worker.name не может иметь значение null.");
        else if (name.length() > 0)
            this.name = name;
        else
            throw new IllegalArgumentException();
    }

    /**
     * Sets new coordinates of this City
     * @param coordinates - new coordinates
     * @throws NullPointerException if coordinates is null
     */
    public void setCoordinates(Coordinates coordinates) throws NullPointerException {
        if (coordinates != null)
            this.coordinates = coordinates;
        else
            throw new NullPointerException("Поле Worker.coordinates не может иметь значение null.");
    }

    /*
     * Sets a new salary of this City
     * @param salary - a new salary
     * @throws InvalidCityAreaException if salary has value 0 or less
     */
    public void setSalary(int salary) throws IllegalArgumentException {
        if (salary > 0)
            this.salary = salary;
        else
            throw new IllegalArgumentException();
    }

    /*
     * Sets a new startDate of this City
     * @param startDate - a new startDate
     * @throws InvalidCityPopulationException if area has value 0 or less
     */
    public void setStartDate(java.time.LocalDate startDate) throws NullPointerException {
        if (startDate != null)
            this.startDate = startDate;
        else
            throw new NullPointerException("Поле Worker.startDate не может иметь значение null.");
    }

    /**
     * Sets a new height above sea level for this City
     * @param position - a new height above sea level
     */
    public void setPosition(Position position) throws NullPointerException {
        this.position = position;
    }

    /**
     * Sets a new status for this City
     * @param status - a new status
     */
    public void setStatus(Status status) throws NullPointerException {
        this.status = status;
    }

    /**
     * Sets a new type of organization in this City
     * @param organization - a new type of organization
     * @throws NullPointerException if organization has null value
     */
    public void setOrganization(Organization organization) throws NullPointerException {
        if (organization != null)
            this.organization = organization;
        else
            throw new NullPointerException("Поле Worker.organization не может иметь значение null.");
    }

    /**
     * Returns the default City for testing purposes
     * @return the default City
     */
    private static Organization defaultOrg = Organization.getInstance("DefaultWorker's Organization", 100, OrganizationType.PUBLIC);
    public static Worker getDefaultWorker() {
        //TODO: remove getDefaultCity
        Coordinates coord = new Coordinates(1, 2);
        return new Worker(1, "c1", coord, 100, java.time.LocalDate.now(), Position.CLEANER, Status.HIRED, defaultOrg);

    }
}

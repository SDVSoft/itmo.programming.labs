package entities.organization.attributes;

/**
 * Possible organization types
 */
public enum OrganizationType {
    COMMERCIAL("коммерческая организация"),
    PUBLIC("общественная организация"),
    TRUST("трест");

    private final String prefix;

    OrganizationType(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() { return prefix; }
}
package collection;

import entities.common.attributes.Coordinates;
import entities.organization.Organization;
import entities.organization.attributes.OrganizationType;
import entities.woker.Worker;
import entities.woker.attributes.Position;
import entities.woker.attributes.Status;
import tools.json.JSONReader;

import com.google.gson.*;
import tools.json.OrganizationAdapter;
import tools.json.ZoneIdAdapter;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.*;

//TODO:Программа должна корректно работать с неправильными данными (ошибки пользовательского ввода, отсутсвие прав доступа к файлу и т.п.).

/**
 * CityCollectionManager is a class that provides the ability to interact with a
 * collection of Cities interactively through the command line.
 */
public class WorkerCollectionManager {
    private ArrayDeque<Worker> workerDeque;
    private java.util.Date initDate;
    private Scanner sc;
    private Scanner mainSc;
    private String filename;
    private Charset charset = StandardCharsets.UTF_8;
    private boolean quiet;
    private boolean scriptExecution;
    private boolean quietScriptOut;
    private ArrayDeque<File> executingScriptsStack;
    private Exception criticalException;
    private int nonCriticalLoadExceptions = 0;
    private IDCounter idCounter;
    private Deque<String> commandHistory;

    /**
     * Initializes a new CityCollectionManager that will be controlled via the "standard"
     * input stream.
     */
    public WorkerCollectionManager() { this(new Scanner(System.in)); }

    /**
     * Initializes a new CityCollectionManager with the specified Scanner
     * @param sc - a Scanner to use.
     */
    public WorkerCollectionManager(Scanner sc) {
        this.workerDeque = new ArrayDeque<>();
        this.initDate = new java.util.Date();
        this.mainSc = this.sc = sc;
        this.quiet = false;
        this.scriptExecution = false;
        this.executingScriptsStack = new ArrayDeque<>();
        this.idCounter = new IDCounter();
        this.commandHistory = new ArrayDeque<>(10);
    }

    //TODO: load
    /**
     * Clears CityHashtable and fills it with Cities from the file.
     */
    protected int load(String filename) {
        int workersLoaded = -1;
        nonCriticalLoadExceptions = 0;
        File loadFile = new File(filename);
        if (!loadFile.isFile()) {
            criticalException = new IOException(filename +
                    " is not a normal file. Loading is forbidden.");
            System.out.println(criticalException.getMessage());
        } else {
            try (FileInputStream fis = new FileInputStream(loadFile)) {
                this.filename = filename;
                workerDeque.clear();
                idCounter.freeAll();
                Worker curWorker;
                //int curWorkerId = 0;
                JSONReader jsonReader = new JSONReader(fis, charset);
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(ZoneId.class, new ZoneIdAdapter());
                gsonBuilder.registerTypeAdapter(Organization.class, new OrganizationAdapter());
                Gson gson = gsonBuilder.create();
                String jsonRow;
                workersLoaded = 0;
                while (jsonReader.hasNext()) {
                    jsonRow = jsonReader.readJSONRow();
                    //try {
                        curWorker = gson.fromJson(jsonRow, Worker.class);
                        //curWorkerId = curWorker.getId();
                        putWithUniqId(curWorker);
                        workersLoaded++;
                    /*} catch (ObjectCreationFailedException ocfe) {
                        nonCriticalLoadExceptions++;
                        System.out.println("Не удалось создать город с ключом " +
                                jsonRow.getValues()[0]);
                        System.out.println(ocfe.getMessage());
                        if (ocfe.getCause() != null)
                            System.out.println("Cause: " + ocfe.getCause().getMessage());
                        System.out.println();
                    } catch (IllegalArgumentException iae) {
                        nonCriticalLoadExceptions++;
                        System.out.println("Не удалось добавить в коллекцию город с ключом " +
                                jsonRow.getValues()[0] + " (id:" + curWorkerId + "):");
                        System.out.println(iae.getMessage());
                        System.out.println();
                    }*/
                }
            } catch (ParseException pe) {
                System.out.println("Не удаётся прочитать файл в формате JSON.");
                System.out.println(pe.getMessage());
                criticalException = pe;
            } catch (FileNotFoundException fnfe) {
                System.out.print("Ошибка при открытии файла ");
                System.out.println(fnfe.getMessage());
                criticalException = fnfe;
            } catch (IOException ioe) {
                System.out.println("При чтении указанного файла возникла " +
                        "непредвиденная ошибка.");
                System.out.println(ioe.getMessage());
                criticalException = ioe;
            }
        }
        String resultMsg = "Загружено сотрудников: " + workersLoaded + ".";
        if (nonCriticalLoadExceptions > 0)
            System.out.println(resultMsg);
        else
            quietPrintln(resultMsg);
        return workersLoaded;
    }

    /**
     * Saves current collection to the file that was previously specified in
     * save(String filename) or in load(String filename)
     * @throws NullPointerException if the file wasn't specified yet
     */
    protected void save() throws NullPointerException {
        if (filename == null)
            throw new NullPointerException("Не указано имя файла для записи.");
        save(filename);
    }
//TODO: save

    /**
     * Saves current collection to the specified file
     * @param filename - the name of the file to store the collection
     */
    protected void save(String filename) {
        int workersSaved = 0;
        try (FileWriter fileWriter = new FileWriter(filename)){
            Gson gson = new Gson();
            this.filename = filename;
            for (Worker w : workerDeque) {
                fileWriter.write(gson.toJson(w) + System.lineSeparator());
                workersSaved++;
            }
            quietPrintln("Коллекция успешно сохранена в файл " + filename);
        } catch (FileNotFoundException fnfe) {
            System.out.print("Ошибка при записи в файл ");
            System.out.println(fnfe.getMessage());
            criticalException = fnfe;
        } catch (IOException ioe) {
            System.out.println("При записи файла " + filename + " возникла " +
                    "непредвиденная ошибка.");
            System.out.println(ioe.getMessage());
            criticalException = ioe;
        }
        quietPrintln("Записано сотрудников: " + workersSaved);
    }

    private String ObjectsToStr(Object[] objs) {
        String[] strEnumValuesArray = new String[objs.length];
        for (int i = 0; i < objs.length; i++)
            strEnumValuesArray[i] = objs[i].toString();
        return String.join(", ", strEnumValuesArray);
    }

    /**
     * Creates a new City from given data
     * @return City that was created
     */
    protected Worker inputElement() {
        return inputElement(null);
    }

    /**
     * Sets new data to the specified City
     //* @param city - the City to update data for
     * @return the specified City  with updated data or a new City with given data if the
     * specified City object was null
     */
    protected Worker inputElement(Worker worker) {
        String curLine;
        String name = null;
        String exceptionMsg = null;
        while (name == null) {
            quietPrintln("Введите имя сотрудника:");
            name = sc.nextLine();
            if (name.equals("")) {
                name = null;
                exceptionMsg = "Недопустимое имя сотрудника: имя сотрудника не может быть пустой строкой.";
                System.out.println(exceptionMsg);
                if (scriptExecution) {
                    criticalException = new IllegalArgumentException(exceptionMsg);
                    return null;
                }
            }
        }
        double coordX = 0;
        boolean correctInput = false;
        while (!correctInput) {
            quietPrintln("Введите координату сотрудника по x (double):");
            if (sc.hasNextDouble()) {
                coordX = sc.nextDouble();
                sc.nextLine();
                correctInput = true;
            } else {
                while (sc.nextLine().replaceAll(" ", "").equals("")) {
                }
                exceptionMsg = "Не удалось определить координату по x. " +
                        "Координата по x должна быть задана числом типа double.";
                System.out.println(exceptionMsg);
                if (scriptExecution) {
                    criticalException = new IllegalArgumentException(exceptionMsg);
                    return null;
                }
            }
        }
        double coordY = 0;
        correctInput = false;
        while (!correctInput) {
            quietPrintln("Введите координату сотрудника по y (double):");
            if (sc.hasNextDouble()) {
                coordY = sc.nextDouble();
                sc.nextLine();
                correctInput = true;
            } else {
                while (sc.nextLine().replaceAll(" ", "").equals("")) {
                }
                exceptionMsg = "Не удалось определить координату по y. " +
                        "Координата по y должна быть задана числом типа double.";
                System.out.println(exceptionMsg);
                if (scriptExecution) {
                    criticalException = new IllegalArgumentException(exceptionMsg);
                    return null;
                }
            }
        }
        Coordinates coordinates = new Coordinates(coordX, coordY);
        int salary = 0;
        correctInput = false;
        exceptionMsg = null;
        while (!correctInput) {
            quietPrintln("Введите зарплату сотрудника (int):");
            if (sc.hasNextInt()) {
                salary = sc.nextInt();
                sc.nextLine();
                if (salary > 0)
                    correctInput = true;
                else
                    exceptionMsg = "Недопустимое значение зарплаты сотрудника: зарплата сотрудника должна быть больше 0.";
            } else {
                while (sc.nextLine().replaceAll(" ", "").equals("")) {
                }
                exceptionMsg = "Не удалось определить зарплату сотрудника.\n" +
                        "Зарплата сотрудника должна быть задана числом типа int.";
            }
            if (!correctInput) {
                System.out.println(exceptionMsg);
                if (scriptExecution) {
                    criticalException = new IllegalArgumentException(exceptionMsg);
                    return null;
                }
            }
        }
        int startDateYear = 0, startDateMonth = 0, startDateDay = 0;
        java.time.LocalDate startDate = null;
        correctInput = false;
        exceptionMsg = null;
        while (!correctInput) {
            quietPrintln("Введите год трудоустройства (int):");
            if (sc.hasNextInt()) {
                startDateYear = sc.nextInt();
                sc.nextLine();
                try {
                    ChronoField.YEAR.checkValidValue(startDateYear);
                    correctInput = true;
                } catch (DateTimeException dte) {
                    exceptionMsg = "Недопустимый год: год должен быть задан числом от " + Year.MIN_VALUE + " до " + Year.MAX_VALUE + ".";
                }
            } else {
                while (sc.nextLine().replaceAll(" ", "").equals("")) {
                }
                exceptionMsg = "Не удалось определить год трудоустройства.\n" +
                        "Год должен быть задан числом типа int.";
            }
            if (!correctInput) {
                System.out.println(exceptionMsg);
                if (scriptExecution) {
                    criticalException = new IllegalArgumentException(exceptionMsg);
                    return null;
                }
            } else {
                correctInput = false;
                exceptionMsg = null;
                while (!correctInput) {
                    quietPrintln("Введите месяц трудоустройства (int):");
                    if (sc.hasNextInt()) {
                        startDateMonth = sc.nextInt();
                        sc.nextLine();
                        if (0 < startDateMonth && startDateMonth <= 12)
                            correctInput = true;
                        else {
                            exceptionMsg = "Недопустимый месяц: месяц должен быть задан числом от 1 до 12.";
                        }
                    } else {
                        while (sc.nextLine().replaceAll(" ", "").equals("")) {
                        }
                        exceptionMsg = "Не удалось определить месяц трудоустройства.\n" +
                                "Месяц должен быть задан числом типа int.";
                    }
                    if (!correctInput) {
                        System.out.println(exceptionMsg);
                        if (scriptExecution) {
                            criticalException = new IllegalArgumentException(exceptionMsg);
                            return null;
                        }
                    }
                }
                correctInput = false;
                exceptionMsg = null;
                while (!correctInput) {
                    quietPrintln("Введите день трудоустройства (int):");
                    if (sc.hasNextInt()) {
                        startDateDay = sc.nextInt();
                        sc.nextLine();
                        correctInput = true;
                    } else {
                        while (sc.nextLine().replaceAll(" ", "").equals("")) {
                        }
                        exceptionMsg = "Не удалось определить день трудоустройства.\n" +
                                "День должен быть задан числом типа int.";
                    }
                    if (!correctInput) {
                        System.out.println(exceptionMsg);
                        if (scriptExecution) {
                            criticalException = new IllegalArgumentException(exceptionMsg);
                            return null;
                        }
                    }
                }
                try {
                    startDate = LocalDate.of(startDateYear, startDateMonth, startDateDay);
                } catch (DateTimeException dte) {
                    exceptionMsg = dte.getMessage();
                    correctInput = false;
                }
                if (!correctInput) {
                    System.out.println(exceptionMsg);
                    if (scriptExecution) {
                        criticalException = new IllegalArgumentException(exceptionMsg);
                        return null;
                    }
                }
            }
        }
        Position position = null;
        correctInput = false;
        String strPositionValues = ObjectsToStr(Position.values());
        while (!correctInput) {
            quietPrintln("Следующий параметр можно не указывать, для этого оставьте " +
                    "строку пустой (без пробельных символов) и нажмите Enter.");
            quietPrintln("Введите должность (Доступные варианты: " +
                    strPositionValues + "):");
            curLine = sc.nextLine().toUpperCase();
            if (curLine.equals("")) correctInput = true;
            else {
                try {
                    position = Position.valueOf(curLine.trim().split(" ")[0]);
                    correctInput = true;
                } catch (IllegalArgumentException iae) {
                    exceptionMsg = "Не удалось определить должность.\n" + iae.getMessage();
                    System.out.println(exceptionMsg);
                    if (scriptExecution) {
                        criticalException = new IllegalArgumentException(exceptionMsg);
                        return null;
                    }
                }
            }
        }
        Status status = null;
        correctInput = false;
        String strStatusValues = ObjectsToStr(Status.values());
        while (!correctInput) {
            quietPrintln("Следующий параметр можно не указывать, для этого оставьте " +
                    "строку пустой (без пробельных символов) и нажмите Enter.");
            quietPrintln("Введите статус сотрудника (Доступные варианты: " +
                    strStatusValues + "):");
            curLine = sc.nextLine().toUpperCase();
            if (curLine.equals("")) correctInput = true;
            else {
                try {
                    status = Status.valueOf(curLine.trim().split(" ")[0]);
                    correctInput = true;
                } catch (IllegalArgumentException iae) {
                    exceptionMsg = "Не удалось определить статус сотрудника.\n" + iae.getMessage();
                    System.out.println(exceptionMsg);
                    if (scriptExecution) {
                        criticalException = new IllegalArgumentException(exceptionMsg);
                        return null;
                    }
                }
            }
        }
        Organization organization = inputOrganization();
        if (organization == null) return null;

        if (worker == null)
            return new Worker(1, name, coordinates, salary, startDate, position, status, organization);
        worker.setName(name);
        worker.setCoordinates(coordinates);
        worker.setSalary(salary);
        worker.setStartDate(startDate);
        worker.setPosition(position);
        worker.setStatus(status);
        worker.setOrganization(organization);
        return worker;
    }

    protected Organization inputOrganization() {
        Organization organization = null;
        boolean correctInput = false;
        String orgName = null;
        String exceptionMsg = null;
        double orgAnnualTurnover = 0;
        OrganizationType orgType = null;
        quietPrintln("Введите название организации:");
        orgName = sc.nextLine();
        organization = Organization.getInstance(orgName);
        if (organization != null) {
            if (!scriptExecution) {
                System.out.println("Данные этой организации уже есть в базе: "
                        + organization.getFullDescription());
                System.out.println("Обновить данные для организации?");
                if (!checkAnswer()) correctInput = true;
            } else {
                correctInput = true;
                sc.nextLine(); sc.nextLine();
            }
        }
        if (!correctInput){
            while (!correctInput) {
                quietPrintln("Введите годовой оборот организации (double):");
                if (sc.hasNextDouble()) {
                    orgAnnualTurnover = sc.nextDouble();
                    sc.nextLine();
                    if (orgAnnualTurnover > 0)
                        correctInput = true;
                    else
                        exceptionMsg = "Недопустимое значение годового оборота организации: годовой оборот должен быть больше 0.";
                } else {
                    while (sc.nextLine().replaceAll(" ", "").equals("")) {
                    }
                    exceptionMsg = "Не удалось определить годовой оборот. " +
                            "Годовой оборот оорганизации должен быть задан числом типа double.";
                }
                if (!correctInput) {
                    System.out.println(exceptionMsg);
                    if (scriptExecution) {
                        criticalException = new IllegalArgumentException(exceptionMsg);
                        return null;
                    }
                }
            }
            correctInput = false;
            String strOrgTypeValues = ObjectsToStr(OrganizationType.values());
            while (!correctInput) {
                quietPrintln("Введите тип организации (Доступные варианты: " +
                        strOrgTypeValues + "):");
                String curLine = sc.nextLine().toUpperCase();
                try {
                    orgType = OrganizationType.valueOf(curLine.trim().split(" ")[0]);
                    correctInput = true;
                } catch (IllegalArgumentException iae) {
                    exceptionMsg = "Не удалось определить тип организации.\n" + iae.getMessage();
                    System.out.println(exceptionMsg);
                    if (scriptExecution) {
                        criticalException = new IllegalArgumentException(exceptionMsg);
                        return null;
                    }
                }
            }
            if (organization != null) {
                organization.setAnnualTurnover(orgAnnualTurnover);
                organization.setType(orgType);
            } else
                organization = Organization.getInstance(orgName, orgAnnualTurnover, orgType);
        }
        return organization;
    }

    /**
     * Prints '\n' only if this.quiet != true
     */
    protected void quietPrintln() { quietPrintln(""); }

    /**
     * Prints the specified string + '\n' only if this.quiet != true
     * @param message - the string to print
     */
    protected void quietPrintln(String message) {
        if (quiet)
            return;
        System.out.println(message);
    }

    /**
     * Prints the specified string only if this.quiet != true
     * @param message - the string to print
     */
    protected void quietPrint(String message) {
        if (quiet)
            return;
        System.out.print(message);
    }

    /**
     * Reads the users answer for some question. And returns true if the answer was "д",
     * "да", "y" or "yes" and false false if the answer was "н", "нет", "n" or "no". If
     * user types something else asks user to input one of this answers again. This method
     * is not case sensitive.
     * @return true if the answer was "д", "да", "y" or "yes" and false false if the
     * answer was "н", "нет", "n" or "no"
     */
    protected boolean checkAnswer() {
        String answer;
        while (true) {
            System.out.println("Введите Д, если да;  Н, если нет:");
            answer = mainSc.nextLine().trim().split(" ")[0].toLowerCase();
            switch (answer) {
                case "д":
                case "да":
                case "y":
                case "yes":
                    return true;
                case "н":
                case "нет":
                case "n":
                case "no":
                    return false;
            }
        }
    }

    protected Worker putWithUniqId(Worker worker) {
        if(!idCounter.lock(worker.getId()))
            worker = new Worker(idCounter.getID(), worker);
        workerDeque.add(worker);
        return worker;
    }

    /**
     * Launches this CityCollectionManager and executes commands from the saved Scanner
     */
    //TODO: save : сохранить коллекцию в файл
    public void work() {
        String[] command;
        Worker worker;
        Iterator<Worker> workerIterator;
        int removed;
        boolean successfulRemove;
        while (sc.hasNext()) {
            command = sc.nextLine().split(" ");
            while (command.length < 1)
                command = sc.nextLine().split(" ");
            command[0] = command[0].toLowerCase();
            commandHistory.push(command[0]);
            if (commandHistory.size() > 10)
                commandHistory.poll();
            switch (command[0]) {
                case "help":
                    try (Scanner cmdHelpSc = new Scanner(new File("./Command help.txt"))) {
                        while (cmdHelpSc.hasNext()) {
                            System.out.println(cmdHelpSc.nextLine());
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("Не найден файл справки по доступным командам.");
                        System.out.println(e.getMessage());
                    }
                    break;
                case "info":
                    System.out.println("Collection info:\n\tType:\n\t\t" + workerDeque.getClass().getSimpleName() +
                            "\n\tInitialization time:\n\t\t" + initDate +
                            "\n\tSize:\n\t\t" + workerDeque.size() + " element" + ((workerDeque.size() == 1) ? "" : "s"));
                    break;
                case "show":
                    for (Worker w : workerDeque) {
                        System.out.println(w.getFullDescription());
                    }
                    break;
                case "add":
                    worker = inputElement();
                    if (worker == null) break;
                    worker = putWithUniqId(worker);
                    quietPrintln("В коллекцию добавлен сотрудник " +
                            worker.getFullDescription());
                    break;
                case "update":
                    if (command.length > 1) {
                        try {
                            int id = Integer.valueOf(command[1]);
                            worker = null;
                            for (Worker w : workerDeque) {
                                if (w.getId() == id) {
                                    worker = w;
                                    break;
                                }
                            }
                            if (worker == null) {
                                String exceptionMsg = "В коллекции нет сотрудника с id=" + id;
                                System.out.println(exceptionMsg);
                                criticalException = new NullPointerException(exceptionMsg);
                            }
                            else {
                                if (inputElement(worker) == null) break;
                                quietPrintln("Данные сотрудника " + worker + " обновлены.");
                                quietPrintln("\nНовые данные сотрудника " +
                                        worker.getFullDescription());
                            }
                        } catch (NumberFormatException nfe) {
                            System.out.println("Не удалось определить id (int).");
                            System.out.println(nfe.getMessage());
                            criticalException = nfe;
                        }
                    } else {
                        System.out.println("Команда update принимает обязательный аргумент id (int).");
                        criticalException = new NullPointerException("Команда update принимает обязательный аргумент id (int).");
                    }
                    break;
                case "remove_by_id":
                    if (command.length > 1) {
                        successfulRemove = false;
                        try {
                            int id = Integer.valueOf(command[1]);
                            workerIterator = workerDeque.iterator();
                            while (!successfulRemove && workerIterator.hasNext()) {
                                if (workerIterator.next().getId() == id) {
                                    workerIterator.remove();
                                    idCounter.free(id);
                                    successfulRemove = true;
                                }
                            }
                            if (successfulRemove)
                                quietPrintln("Из коллекции удален сотрудник с id=" + id);
                            else
                                quietPrintln("В коллекции нет сотрудника с id=" + id);
                        } catch (NumberFormatException nfe) {
                            System.out.println("Не удалось определить id (int).");
                            System.out.println(nfe.getMessage());
                            criticalException = nfe;
                        }
                    } else {
                        System.out.println("Команда update принимает обязательный аргумент id (int).");
                        criticalException = new NullPointerException("Команда update принимает обязательный аргумент id (int).");
                    }
                    break;
                case "clear":
                    workerDeque.clear();
                    idCounter.freeAll();
                    quietPrintln("Все сотрудники из коллекции удалены.");
                    break;
                case "save":
                    if (filename == null) {
                        System.out.println("Не указано имя файла для записи.");
                        System.out.println("Воспользуйтесь командой save_as с указанием имени файла.");
                        criticalException = new NullPointerException("Не указано имя файла для записи.");
                    } else
                        save();
                    break;
                case "save_as":
                    if (command.length > 1)
                        save(String.join(" ", Arrays.copyOfRange(command, 1, command.length)));
                    else {
                        System.out.println("Команда save_as принимает обязательный аргумент file_name (String).");
                        criticalException = new NullPointerException("Команда save_as принимает обязательный аргумент file_name (String).");
                    }
                    break;
                case "load":
                    if (command.length > 1) {
                        String loadFilename = String.join(" ", Arrays.copyOfRange(command, 1, command.length));
                        TODO: load(loadFilename);
                        if (nonCriticalLoadExceptions > 0 && scriptExecution) {
                            System.out.println("Загрузка коллекции из файла " +
                                    loadFilename + " прошла с ошибками.");
                            System.out.println("Количество ошибок: " + nonCriticalLoadExceptions);
                            System.out.println("Продолжить выполнение скрипта?");
                            if (!checkAnswer()) {
                                quietScriptOut = true;
                                return;
                            }
                        }
                    }
                    else {
                        System.out.println("Команда load принимает обязательный аргумент file_name (String).");
                        criticalException = new NullPointerException("Команда load принимает обязательный аргумент file_name (String).");
                    }
                    break;
                case "execute_script":
                    if (command.length > 1) {
                        String exeFilename = String.join(" ", Arrays.copyOfRange(command, 1, command.length));
                        File exeFile = new File(exeFilename);
                        if (!exeFile.isFile()) {
                            criticalException = new IOException(exeFilename +
                                    " is not a normal file. Execution is forbidden.");
                            System.out.println(criticalException.getMessage());
                        } else if (executingScriptsStack.contains(exeFile)) {
                            criticalException = new IOException(exeFilename +
                                    " уже выполняется. Цикличный вызов скриптов запрещён.");
                            System.out.println(criticalException.getMessage());
                        } else {
                            Scanner curScanner = sc;
                            boolean curScriptExecution = scriptExecution;
                            boolean curQuiet = quiet;
                            try (Scanner exeFileSc = new Scanner(exeFile)) {
                                sc = exeFileSc;
                                executingScriptsStack.push(exeFile);
                                scriptExecution = quiet = true;
                                criticalException = null;
                                quietScriptOut = false;
                                work();
                                quiet = curQuiet;
                                scriptExecution = curScriptExecution;
                                executingScriptsStack.pop();
                                sc = curScanner;
                                if (criticalException != null)
                                    System.out.println("Выполнение скрипта прервано.");
                                else if (quietScriptOut)
                                    quietPrintln("Выполнение скрипта прервано пользователем.");
                                else
                                    quietPrintln("Выполнение скрипта завершено.");
                            } catch (FileNotFoundException fnfe) {
                                System.out.print("Ошибка при открытии файла ");
                                System.out.println(fnfe.getMessage());
                                criticalException = fnfe;
                            }
                        }
                    } else {
                        System.out.println("Команда execute_script принимает обязательный аргумент file_name (String).");
                        criticalException = new NullPointerException("Команда execute_script принимает обязательный аргумент file_name (String).");
                    }
                    break;
                case "exit":
                    if (scriptExecution)
                        return;
                    System.exit(0);
                case "remove_greater":
                    worker = inputElement();
                    if (worker == null) break;
                    removed = 0;
                    workerIterator = workerDeque.iterator();
                    while (workerIterator.hasNext()) {
                        if (worker.compareTo(workerIterator.next()) < 0) {
                            workerIterator.remove();
                            removed++;
                        }
                    }
                    quietPrintln("Удалено сотрудников: " + removed);
                    break;
                case "remove_lower":
                    worker = inputElement();
                    if (worker == null) break;
                    removed = 0;
                    workerIterator = workerDeque.iterator();
                    while (workerIterator.hasNext()) {
                        if (worker.compareTo(workerIterator.next()) > 0) {
                            workerIterator.remove();
                            removed++;
                        }
                    }
                    quietPrintln("Удалено сотрудников: " + removed);
                    break;
                case "history":
                    for (String cmd : commandHistory)
                        System.out.println("\t" + cmd);
                    break;
                case "remove_any_by_organization":
                    Organization organization = inputOrganization();
                    if (organization == null) break;
                    successfulRemove = false;
                    workerIterator = workerDeque.iterator();
                    while (!successfulRemove && workerIterator.hasNext()) {
                        worker = workerIterator.next();
                        if (worker.getOrganization() == organization) {
                            workerIterator.remove();
                            idCounter.free(worker.getId());
                            successfulRemove = true;
                        }
                    }
                    if (successfulRemove)
                        quietPrintln("Из коллекции удален сотрудник организации " + organization);
                    else
                        quietPrintln("В коллекции нет ни одного сотрудника организации " + organization);
                    break;
                case "count_by_salary":
                    if (command.length > 1) {
                        try {
                            int salary = Integer.valueOf(command[1]);
                            int count = 0;
                            for (Worker w : workerDeque)
                                if (w.getSalary() == salary) count++;
                            System.out.println("Количество сотрудников с зарплатой " + salary + ": " + count);
                        } catch (NumberFormatException nfe) {
                            System.out.println("Не удалось определить salary (int).");
                            System.out.println(nfe.getMessage());
                            criticalException = nfe;
                        }
                    } else {
                        System.out.println("Команда count_by_salary принимает обязательный аргумент salary (int).");
                        criticalException = new NullPointerException("Команда count_by_salary принимает обязательный аргумент salary (int).");
                    }
                    break;
                case "count_less_than_status":
                    if (command.length > 1) {
                        try {
                            Status status = Status.valueOf(command[1].toUpperCase());
                            int count = 0;
                            for (Worker w : workerDeque)
                                if (w.getStatus().compareTo(status) < 0) count++;
                            System.out.println("Количество сотрудников статус которых ниже " + status + ": " + count);
                        } catch (IllegalArgumentException iae) {
                            String msg = "Не удалось определить статус.\n" + iae.getMessage();
                            System.out.println(msg);
                            quietPrintln("Доступные варианты: " + ObjectsToStr(Status.values()));
                            criticalException = new NullPointerException(msg);
                        }
                    } else {
                        System.out.println("Команда count_less_than_status принимает обязательный аргумент status (String).");
                        criticalException = new NullPointerException("Команда count_less_than_status принимает обязательный аргумент status (String).");
                    }
                    break;
                default:
                    commandHistory.pop();
            }
            if (criticalException != null && scriptExecution) {
                System.out.println("Ошибка при выполнении команды: " +
                        String.join(" ", command));
                return;
            }
            if (quietScriptOut && scriptExecution) return;
        }
    }

    /**
     * Main method to start the application
     * @param args - command line parameters
     */
    public static void main(String[] args) {
        WorkerCollectionManager wcm = new WorkerCollectionManager();
        if (args.length > 0) {
            String path = "";
            if (args[0].equals("-filename")) {
                if (args.length > 1)
                    path = args[1];
            } else {
                path = System.getenv(args[0]);
            }
            System.out.println(path);
            if (path == null || wcm.load(path) < 0)
                System.out.println("Воспользуйтесь командой load, чтобы загрузить коллекцию.");
        }
        wcm.work();
    }
}

package collection;

import java.util.HashSet;

public class IDCounter {
    private HashSet<Integer> lockedIDs;
    private int next;

    public IDCounter() {
        lockedIDs = new HashSet<>();
        next = 1;
    }

    public int getID() throws IndexOutOfBoundsException {
        if (lockedIDs.size() == Integer.MAX_VALUE) throw new IndexOutOfBoundsException("Ran out of free IDs.");
        while (lockedIDs.contains(next)) {
            while (lockedIDs.contains(next)) next++;
            if (next < 0) next = 1;
        }
        lockedIDs.add(next);
        return next;
    }

    public boolean lock(int id) {
        if (!isLocked(id)) {
            lockedIDs.add(id);
            return true;
        }
        return false;
    }

    public void free(int id) {
        lockedIDs.remove(id);
    }

    public void freeAll() {
        lockedIDs.clear();
    }

    public boolean isLocked(int id) {
        return lockedIDs.contains(id);
    }
}

package tools.json;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

public class InputStreamLineReader {
    private BufferedInputStream bufferedIS;
    private Charset charset;
    private byte[][] lineSeparatorsBytes;
    private byte[] lfBytes;
    private static final String[] LINE_SEPARATORS = "\n\r\u2028\u2029\u0085".split("");
    private String lastLineSeparator;

    public InputStreamLineReader(InputStream is) { this(is, Charset.defaultCharset()); }

    public InputStreamLineReader(InputStream is, String charsetName) throws UnsupportedCharsetException {
        this(is, Charset.forName(charsetName));
    }

    public InputStreamLineReader(InputStream is, Charset charset) {
        this.bufferedIS = new BufferedInputStream(is);
        this.charset = charset;
        lineSeparatorsBytes = new byte[LINE_SEPARATORS.length][];
        for (int i = 0; i < LINE_SEPARATORS.length; i++)
            lineSeparatorsBytes[i] = LINE_SEPARATORS[i].getBytes(charset);
        lfBytes = "\n".getBytes(charset);
    }

    public boolean hasNext() {
        bufferedIS.mark(1);
        try {
            int b = bufferedIS.read();
            bufferedIS.reset();
            return b != -1;
        } catch (IOException ioe) {
            return false;
        }
    }

    public String getLastLineSeparator() { return lastLineSeparator; }

    public String readLine() throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int b, prevB;
            boolean endOfLine = false;
            while (!endOfLine && (b = bufferedIS.read()) != -1) {
                for (byte[] bytesOfLineSep : lineSeparatorsBytes) {
                    if (b == bytesOfLineSep[0]) {
                        bufferedIS.mark(4);
                        prevB = b;
                        endOfLine = true;
                        for (int i = 1; i < bytesOfLineSep.length && endOfLine; i++) {
                            if ((b = bufferedIS.read()) != bytesOfLineSep[i])
                                endOfLine = false;
                        }
                        if (endOfLine) {
                            lastLineSeparator = new String(bytesOfLineSep, charset);
                            if (lastLineSeparator.equals("\r")) {
                                boolean crlfLineSep = true;
                                bufferedIS.mark(4);
                                for (int i = 0; i < lfBytes.length && crlfLineSep; i++) {
                                    if ((b = bufferedIS.read()) != lfBytes[i])
                                        crlfLineSep = false;
                                }
                                if (crlfLineSep) lastLineSeparator = "\r\n";
                                else bufferedIS.reset();
                            }
                            break;
                        } else {
                            b = prevB;
                            bufferedIS.reset();
                        }
                    }
                }
                if (!endOfLine)
                    out.write(b);
            }
            return out.toString(charset.name());
        }
    }
}

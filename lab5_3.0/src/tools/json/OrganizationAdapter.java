package tools.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import entities.organization.Organization;
import entities.organization.attributes.OrganizationType;

import java.lang.reflect.Type;

public class OrganizationAdapter implements JsonDeserializer<Organization> {
    public Organization deserialize(JsonElement jsonElement, Type type,
                              JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        String orgName = jsonElement.getAsJsonObject().get("fullName").getAsString();
        if (Organization.exists(orgName))
            return Organization.getInstance(orgName);
        double annualTurnover = Double.valueOf(jsonElement.getAsJsonObject().get("annualTurnover").getAsString());
        OrganizationType orgType = OrganizationType.valueOf(jsonElement.getAsJsonObject().get("type").getAsString());
        return Organization.getInstance(orgName, annualTurnover, orgType);
    }
}

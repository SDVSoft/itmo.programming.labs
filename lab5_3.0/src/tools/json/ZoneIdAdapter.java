package tools.json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.ZoneId;

public class ZoneIdAdapter implements /*JsonSerializer<ZoneId>,*/ JsonDeserializer<ZoneId> {

    private static final String CLASSNAME = "CLASSNAME";
    private static final String DATA = "DATA";

    public ZoneId deserialize(JsonElement jsonElement, Type type,
                         JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        //JsonObject jsonObject = jsonElement.getAsJsonObject().get("id").getAsString();
        //JsonPrimitive prim = (JsonPrimitive) jsonObject.get("id");
        String zoneId = jsonElement.getAsJsonObject().get("id").getAsString();
        //Class klass = getObjectClass("ZoneRegion");
        return ZoneId.of(zoneId);
    }
/*
    public JsonElement serialize(ZoneId jsonElement, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(CLASSNAME, jsonElement.getClass().getName());
        jsonObject.add(DATA, jsonSerializationContext.serialize(jsonElement));
        return jsonObject;
    }*/
    /****** Helper method to get the className of the object to be deserialized *****/
    public Class getObjectClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }
    }
}
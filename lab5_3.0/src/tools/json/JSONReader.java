package tools.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Stack;

public class JSONReader {
    private static final char[] open_brackets = {'\"', '[', '{'};
    private static final char[] close_brackets = {'\"', ']', '}'};
    private Scanner sc;
    private InputStreamLineReader isLineReader;

    public JSONReader(Scanner sc) { this.sc = sc; }

    public JSONReader(InputStream is) { this(is, Charset.defaultCharset()); }

    public JSONReader(InputStream is, String charsetName) throws UnsupportedCharsetException {
        this(is, Charset.forName(charsetName));
    }

    public JSONReader(InputStream is, Charset charset) {
        this.isLineReader = new InputStreamLineReader(is, charset);
    }

    private static boolean isPairedBrackets(char open_bracket, char close_bracket) {
        if (open_bracket == '\"' && close_bracket == '\"') return true;
        if (open_bracket == '{' && close_bracket == '}') return true;
        return open_bracket == '[' && close_bracket == ']';
    }

    private String nextLine() throws IOException {
        if (sc != null) return sc.nextLine();
        return isLineReader.readLine();
    }

    public boolean hasNext() {
        if (sc != null)
            return sc.hasNext();
        return isLineReader.hasNext();
    }

    public String readJSONRow() throws ParseException {
        StringBuilder res = new StringBuilder();
        Stack<Character> brackets = new Stack<>();
        boolean end_reached = false, escape_char = false;
        String line;
        while (!end_reached) {
            try {
                if (!hasNext()) throw new NoSuchElementException("No line found");
                line = nextLine();
            } catch (UnsupportedEncodingException uee) {
                ParseException pe = new ParseException("Ошибка при чтении JSON: указана неподдерживаемая кодировка.", 0);
                pe.initCause(uee);
                throw pe;
            } catch (IOException | NoSuchElementException e) {
                ParseException pe = new ParseException("Ошибка при чтении JSON: незавершённый объект", res.length());
                pe.initCause(e);
                throw pe;
            }
            if (!brackets.empty() && brackets.peek() == '\"') res.append("\n");
            char[] char_arr = line.toCharArray();
            for (int i = 0; i < char_arr.length; i++) {
                if (escape_char) escape_char = false;
                else if (char_arr[i] == '\\') escape_char = true;
                else if (!brackets.empty() && brackets.peek() == '\"') {
                    if (char_arr[i] == '\"') brackets.pop();
                }
                else if (Arrays.binarySearch(open_brackets, char_arr[i]) >= 0)
                    brackets.push(char_arr[i]);
                else if (Arrays.binarySearch(close_brackets, char_arr[i]) >= 0) {
                    if (!brackets.empty() && isPairedBrackets(brackets.peek(), char_arr[i])) {
                        brackets.pop();
                        if (brackets.empty()) {
                            end_reached = true;
                            line = line.substring(0, i + 1);
                        }
                    } else
                        throw new ParseException("Ошибка при чтении JSON: неправильная скобочная последовательность", res.length() + i);
                }
            }
            res.append(line);
        }
        return res.toString();
    }

    public static void main(String[] args) throws ParseException {
        JSONReader jr = new JSONReader(new Scanner(System.in));
        System.out.println(jr.readJSONRow());
    }
}
